//
//  UpdateViewController.h
//  LibOnFon
//
//  Created by Kunal Shrivastava on 1/16/15.
//  Copyright (c) 2015 Kunal Shrivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *bookTitleText;
@property (strong, nonatomic) IBOutlet UITextField *authorText;
@property (strong, nonatomic) IBOutlet UITextField *publisherText;
@property (strong, nonatomic) IBOutlet UITextField *categoriesText;
- (IBAction)updateButton:(id)sender;

@property (strong, nonatomic) NSString *bookName;
@property (strong, nonatomic) NSString *publisher;
@property (strong, nonatomic) NSString *author;
@property (strong, nonatomic) NSString *categories;
@property (strong, nonatomic) NSString *url;

@end
