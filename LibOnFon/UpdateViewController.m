//
//  UpdateViewController.m
//  LibOnFon
//
//  Created by Kunal Shrivastava on 1/16/15.
//  Copyright (c) 2015 Kunal Shrivastava. All rights reserved.
//

#import "UpdateViewController.h"

@interface UpdateViewController ()

@end

@implementation UpdateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bookTitleText.text=self.bookName;
    self.publisherText.text=self.publisher;
    self.authorText.text=self.author;
    self.categoriesText.text=self.categories;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)updateButton:(id)sender
{
    if([self.bookTitleText.text isEqualToString:@""] || self.bookTitleText.text==nil || [self.authorText.text isEqualToString:@""] || self.authorText.text==nil)
    {
        
        NSString *message = @"Either Title or Author is not entered. Please check.";
        UIAlertView *invalidInputMessage = [[UIAlertView alloc] initWithTitle:@"Invalid input"
                                                                      message:message
                                                                     delegate:self
                                                            cancelButtonTitle:@"Ok"
                                                            otherButtonTitles:nil];
        [invalidInputMessage show];
    }
    else
    {
        NSError *error;
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        
        //NSURL *url = [NSURL URLWithString:@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915/books/"];
        
        NSMutableString *urlupdate=[NSMutableString stringWithString:@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915"];
        //@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915/books/";
        //NSString *urldelbook = [urldel stringByAppendingString:delrow];
        [urlupdate appendString:self.url];
        NSURL *urlupdatebook = [NSURL URLWithString:urlupdate];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlupdatebook
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"PUT"];
        NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: self.authorText.text,@"author" ,
                                 self.categoriesText.text,@"categories" ,
                                 self.bookTitleText.text,@"title" ,
                                 self.publisherText.text,@"publisher" ,
                                 nil];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
        
        NSString* jsonString = [[NSString alloc] initWithBytes:[postData bytes] length:[postData length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"Dict:%@", jsonString);
        
        [request setHTTPBody:postData];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"result string: %@", newStr);
            
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"result json: %@", jsonArray);
            
            if (error != nil) {
                // If any error occurs then just display its description on the console.
                NSLog(@"%@", [error localizedDescription]);
            }
            else{
                // If no error occurs, check the HTTP status code.
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                
                // If it's other than 200, then show it on the console.
                if (HTTPStatusCode != 200) {
                    NSLog(@"HTTP status code = %ld", (long)HTTPStatusCode);
                }
                // Call the completion handler with the returned data on the main thread.
                //[[NSOperationQueue mainQueue] addOperationWithBlock:^{
                //completionHandler(data);
                //}];
                //[self dismissViewControllerAnimated:YES completion:nil];
            }
            
        }];
        
        [postDataTask resume];
    }

}
@end
