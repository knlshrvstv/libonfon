//
//  AppDelegate.h
//  LibOnFon
//
//  Created by Kunal Shrivastava on 12/30/14.
//  Copyright (c) 2014 Kunal Shrivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+(void)fetchBooks:(NSURL *)url withCompletionHandler:(void(^)(NSData *data))completionHandler;


@end

