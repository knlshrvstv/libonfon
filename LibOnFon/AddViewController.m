//
//  AddViewController.m
//  LibOnFon
//
//  Created by Kunal Shrivastava on 12/30/14.
//  Copyright (c) 2014 Kunal Shrivastava. All rights reserved.
//

#import "AddViewController.h"

@interface AddViewController ()

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.delegate fromAddViewController];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Button actions
- (IBAction)submitButton:(id)sender
{
    if([self.titleLabel.text isEqualToString:@""] || self.titleLabel.text==nil)
    {
        
        NSString *message = @"Either Title or Author is not entered. Please check.";
        UIAlertView *invalidInputMessage = [[UIAlertView alloc] initWithTitle:@"Invalid input"
                                                                        message:message
                                                                       delegate:self
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
        [invalidInputMessage show];
    }
    else
    {
        NSError *error;
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        
        NSURL *url = [NSURL URLWithString:@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915/books/"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"POST"];
        NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: self.authorLabel.text,@"author" ,
                                 self.categoriesLabel.text,@"categories" ,
                                 self.titleLabel.text,@"title" ,
                                 self.publisherLabel.text,@"publisher" ,
                                 nil];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
        
        NSString* jsonString = [[NSString alloc] initWithBytes:[postData bytes] length:[postData length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"Dict:%@", jsonString);
        
        [request setHTTPBody:postData];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"result string: %@", newStr);
            
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"result json: %@", jsonArray);
            
            if (error != nil) {
                // If any error occurs then just display its description on the console.
                NSLog(@"%@", [error localizedDescription]);
            }
            else
            {
                // If no error occurs, check the HTTP status code.
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                
                // If it's other than 200, then show it on the console.
                if (HTTPStatusCode != 200) {
                    NSLog(@"HTTP status code = %ld", (long)HTTPStatusCode);
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.titleLabel.text=@"";
                    self.publisherLabel.text=@"";
                    self.authorLabel.text=@"";
                    self.categoriesLabel.text=@"";                                              });
                
                // Call the completion handler with the returned data on the main thread.
                //[[NSOperationQueue mainQueue] addOperationWithBlock:^{
                //completionHandler(data);
                //}];
            }
            
        }];
        
        [postDataTask resume];
    }
    

}
- (IBAction)doneButton:(id)sender
{
    if(![self.titleLabel.text isEqualToString:@""] || self.authorLabel.text!=nil || ![self.authorLabel.text isEqualToString:@""] || self.authorLabel.text!=nil || ![self.publisherLabel.text isEqualToString:@""] || self.publisherLabel.text!=nil || ![self.categoriesLabel.text isEqualToString:@""] || self.categoriesLabel.text!=nil)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"You have unsaved contents here. Discard them?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        [alert show];
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == 1 )
    {
       [self dismissViewControllerAnimated:NO completion:nil];
    }
}





@end
