//
//  MainTableViewController.m
//  LibOnFon
//
//  Created by Kunal Shrivastava on 12/30/14.
//  Copyright (c) 2014 Kunal Shrivastava. All rights reserved.
//

#import "MainTableViewController.h"
#import "DetailsViewController.h"


@interface MainTableViewController ()

@end

@implementation MainTableViewController

BOOL fromDetails=NO;
BOOL fromAdd=NO;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBook)];
    self.navigationItem.leftBarButtonItem=self.addButton;
    self.navigationItem.rightBarButtonItem=self.sweepButtoncust;
    //self.sweepButtoncust.image=[UIImage imageNamed:@"sweep.png"];
    self.errorLabel.hidden=YES;
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Library_bg"]];
    [self.tableView setBackgroundView:tempImageView];
    [self fetchBooksHUD];
}

-(void)viewDidAppear:(BOOL)animated
{
    /*
    if(fromDetails)
    {
        fromDetails=NO;
        [self fetchBooksHUD];
    }
    if(fromAdd)
    {
        fromAdd=NO;
        [self fetchBooksHUD];
    }*/
    [self fetchBooksHUD];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Fetch methods implementation

-(void)fetchBooksHUD{

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        
        // Regiser for HUD callbacks so we can remove it from the window at the right time
        HUD.delegate = self;
        HUD.labelText = @"Looking for books";
        
        // Show the HUD while the provided method executes in a new thread
        [HUD showWhileExecuting:@selector(fetchBooks) onTarget:self withObject:nil animated:YES];
        
        // update UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    });
}

-(void)fetchBooks
{
    NSError* error;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915/books/"]];
    [request setHTTPMethod:@"GET"];
    
    
    NSURLResponse *requestResponse;
    
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:&error];
    //NSData *requestHandler = [NSURLConnection send:request returningResponse:&requestResponse error:nil];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
    NSLog(@"requestReply: %@", requestReply);
    
    if (error != nil) {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    else
    {
        // If no error occurs, check the HTTP status code.
        NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)requestResponse statusCode];
        
        // If it's other than 200, then show it on the console.
        if (HTTPStatusCode != 200) {
            NSLog(@"HTTP status code = %ld", (long)HTTPStatusCode);
        }
    }

    
    books = [NSJSONSerialization
                     JSONObjectWithData:requestHandler //1
             
                     options:kNilOptions
                     error:&error];
    [self.tableView reloadData];
    //NSDictionary *st=[[NSDictionary alloc] initWithDictionary:[json objectAtIndex:0]];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return books.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BooksCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 2.0; //seconds
    //lpgr.cancelsTouchesInView=NO;
    [cell addGestureRecognizer:lpgr];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *st=[[NSDictionary alloc] initWithDictionary:[books objectAtIndex:indexPath.row]];
    
    cell.textLabel.text = [st objectForKey: @"title"];
    cell.detailTextLabel.text=[st objectForKey: @"author"];
    
    return cell;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Delete Book" message:@"Are you sure you want to delete this book?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        [alert setTag:indexPath.row];
        [alert show];
        [self setDeleteIndex:indexPath];
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DetailsSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DetailsViewController *detailsViewController = segue.destinationViewController;
        detailsViewController.delegate=self;
        if([[books objectAtIndex:indexPath.row] objectForKey: @"title"]==[NSNull null])
        {
            detailsViewController.bookName = @"-";
        }
        else
        {
            detailsViewController.bookName = [[books objectAtIndex:indexPath.row] objectForKey: @"title"];
        }
        
        if([[books objectAtIndex:indexPath.row] objectForKey: @"publisher"]==[NSNull null])
        {
            detailsViewController.publisher = @"-";
        }
        else
        {
            detailsViewController.publisher = [[books objectAtIndex:indexPath.row] objectForKey: @"publisher"];
        }
        
        if([[books objectAtIndex:indexPath.row] objectForKey: @"categories"]==[NSNull null])
        {
            detailsViewController.publisher = @"-";
        }
        else
        {
            detailsViewController.tags = [[books objectAtIndex:indexPath.row] objectForKey: @"categories"];
        }
        
        
        
        if([[books objectAtIndex:indexPath.row] objectForKey: @"lastCheckedOutBy"]==[NSNull null] || [[books objectAtIndex:indexPath.row] objectForKey: @"lastCheckedOut"]==[NSNull null])
        {
            detailsViewController.checkout = [NSString stringWithFormat:@"%@@%@",@"-",@"-"];
        }
        else
        {
            detailsViewController.checkout = [NSString stringWithFormat:@"%@@%@",[[books objectAtIndex:indexPath.row] objectForKey: @"lastCheckedOutBy"],[[books objectAtIndex:indexPath.row] objectForKey: @"lastCheckedOut"]];
        }
        
        detailsViewController.bookurl=[[books objectAtIndex:indexPath.row] objectForKey: @"url"];
        
    }

}

- (IBAction)unwindFromDetails:(UIStoryboardSegue*)sender
{
    UIViewController* sourceViewController = sender.sourceViewController;
    
    if ([sourceViewController isKindOfClass:[DetailsViewController class]])
    {
        NSLog(@"Coming from BLUE!");
        [self fetchBooksHUD];
    }
    
}


#pragma mark - Network checks

//Checks if the network is available
- (BOOL) connectedToNetwork
{
    Reachability *r = [Reachability reachabilityWithHostname:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    BOOL internet;
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)) {
        internet = NO;
    } else {
        internet = YES;
    }
    return internet;
}

#pragma mark - DetailsViewControllerDelegate

- (void)fromDetailsViewController
{
    fromDetails=YES;
}

#pragma mark - DetailsViewControllerDelegate

- (void)fromAddViewController
{
    fromAdd=YES;
}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == 1 )
    {
        NSDictionary *st=[[NSDictionary alloc] initWithDictionary:[books objectAtIndex:self.deleteIndex.row]];
        
        NSMutableString *delrow = [st objectForKey:@"url"];
        
        NSError* error;
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSMutableString *urldelbook=[NSMutableString stringWithString:@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915"];
        //@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915/books/";
        //NSString *urldelbook = [urldel stringByAppendingString:delrow];
        [urldelbook appendString:delrow];
        [request setURL:[NSURL URLWithString:urldelbook]];
        [request setHTTPMethod:@"DELETE"];
        
        NSURLResponse *requestResponse;
        
        NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:&error];
        //NSData *requestHandler = [NSURLConnection send:request returningResponse:&requestResponse error:nil];
        
        NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
        NSLog(@"requestReply: %@", requestReply);
        
        books = [NSJSONSerialization
                 JSONObjectWithData:requestHandler //1
                 
                 options:kNilOptions
                 error:&error];
        [self fetchBooksHUD];


    }
}

#pragma mark touch handlers

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath == nil) {
        NSLog(@"long press on table view but not on a row");
    }
    else
    {
        if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
        {
            NSLog(@"long press on table view at row %ld", (long)indexPath.row);
            
            UpdateViewController *editView = [self.storyboard instantiateViewControllerWithIdentifier:@"editView"]; //dont forget to set storyboard ID of you editViewController in storyboard
            
            if([[books objectAtIndex:indexPath.row] objectForKey: @"title"]==[NSNull null])
            {
                editView.bookName = @"-";
            }
            else
            {
                editView.bookName = [[books objectAtIndex:indexPath.row] objectForKey: @"title"];
            }
            
            if([[books objectAtIndex:indexPath.row] objectForKey: @"publisher"]==[NSNull null])
            {
                editView.publisher = @"-";
            }
            else
            {
                editView.publisher = [[books objectAtIndex:indexPath.row] objectForKey: @"publisher"];
            }
            
            if([[books objectAtIndex:indexPath.row] objectForKey: @"categories"]==[NSNull null])
            {
                editView.categories = @"-";
            }
            else
            {
                editView.categories = [[books objectAtIndex:indexPath.row] objectForKey: @"categories"];
            }
            if([[books objectAtIndex:indexPath.row] objectForKey: @"author"]==[NSNull null])
            {
                editView.author = @"-";
            }
            else
            {
                editView.author = [[books objectAtIndex:indexPath.row] objectForKey: @"author"];
            }
            if([[books objectAtIndex:indexPath.row] objectForKey: @"url"]==[NSNull null])
            {
                editView.url = @"-";
            }
            else
            {
                editView.url = [[books objectAtIndex:indexPath.row] objectForKey: @"url"];
            }
            
            [self.navigationController pushViewController:editView animated:YES];
        }
    }
    
    
}

- (IBAction)sweepButton:(id)sender
{
    NSError* error;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSMutableString *urldelbook=[NSMutableString stringWithString:@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915/clean"];
    [request setURL:[NSURL URLWithString:urldelbook]];
    [request setHTTPMethod:@"DELETE"];
    
    NSURLResponse *requestResponse;
    
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:&error];
    //NSData *requestHandler = [NSURLConnection send:request returningResponse:&requestResponse error:nil];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
    NSLog(@"requestReply: %@", requestReply);
    
    books = [NSJSONSerialization
             JSONObjectWithData:requestHandler //1
             
             options:kNilOptions
             error:&error];
    [self fetchBooksHUD];

}
@end
