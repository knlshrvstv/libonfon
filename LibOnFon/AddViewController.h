//
//  AddViewController.h
//  LibOnFon
//
//  Created by Kunal Shrivastava on 12/30/14.
//  Copyright (c) 2014 Kunal Shrivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddViewControllerDelegate <NSObject>
- (void)fromAddViewController;
@end

@interface AddViewController : UIViewController

@property (nonatomic, weak) id <AddViewControllerDelegate> delegate;

- (IBAction)submitButton:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *authorLabel;
@property (strong, nonatomic) IBOutlet UITextField *publisherLabel;
@property (strong, nonatomic) IBOutlet UITextField *categoriesLabel;
- (IBAction)doneButton:(id)sender;

@end
