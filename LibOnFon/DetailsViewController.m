//
//  DetailsViewController.m
//  LibOnFon
//
//  Created by Kunal Shrivastava on 12/30/14.
//  Copyright (c) 2014 Kunal Shrivastava. All rights reserved.
//

#import "DetailsViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "MBProgressHUD.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

NSString *userName;
NSString *dateString;
NSError *errorchkout;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userName=[[NSString alloc] init];
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(fbShare)];
    //UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    self.navigationController.navigationBarHidden=NO;
    //self.navigationItem.leftBarButtonItem=backButton;
    self.navigationItem.rightBarButtonItem=shareButton;
    self.booknameLabel.text=self.bookName;
    self.publisherLabel.text=self.publisher;
    self.tagsLabel.text=self.tags;
    self.checkoutLabel.text=self.checkout;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.delegate fromDetailsViewController];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - facebook share

-(void)fbShare
{
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:@"https://developers.facebook.com/docs/ios/share/"];
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        
        // Present share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
        
        // If the Facebook app is NOT installed and we can't present the share dialog
    } else {
        // FALLBACK: publish just a link using the Feed dialog
        
        // Put together the dialog parameters
        
        NSMutableString *sharemsg=[NSMutableString stringWithString:@"I like the book: "];
        //@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915/books/";
        //NSString *urldelbook = [urldel stringByAppendingString:delrow];
        [sharemsg appendString:self.bookName];

        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"LibOnFon", @"name",
                                       @"Your phone. Your library", @"caption",
                                       sharemsg, @"description",
                                       @"https://developers.facebook.com/docs/ios/share/", @"link",
                                       @"http://i.imgur.com/g3Qc1HN.png", @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          NSLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User canceled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User canceled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    }

}

//------------------------------------

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query
{
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

//------------------------------------



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
# pragma mark - Check out request
-(void)checkoutBook
{
     NSError *error;
     
     NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
     NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
     NSString *basicurl=[[NSString alloc] initWithFormat:@"http://prolific-interview.herokuapp.com/54a2fb62be9730000793b915%@",self.bookurl];
     NSURL *url = [NSURL URLWithString:basicurl];
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
     cachePolicy:NSURLRequestUseProtocolCachePolicy
     timeoutInterval:60.0];
     
     [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
     [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
     
     [request setHTTPMethod:@"PUT"];
     dateString = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterFullStyle];
     NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: userName,@"lastCheckedOutBy" ,
     dateString,@"lastCheckedOut",
     nil];
     
     NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
     
     NSString* jsonString = [[NSString alloc] initWithBytes:[postData bytes] length:[postData length] encoding:NSUTF8StringEncoding];
     
     NSLog(@"Dict:%@", jsonString);
     
     [request setHTTPBody:postData];
    
    
     NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
     
     
     NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
     NSLog(@"result string: %@", newStr);
     
     NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
     NSLog(@"result json: %@", jsonArray);
     
     errorchkout=error;
     if (error != nil) {
     // If any error occurs then just display its description on the console.
     NSLog(@"%@", [error localizedDescription]);
     }
     else{
     // If no error occurs, check the HTTP status code.
     NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
     
     // If it's other than 200, then show it on the console.
     if (HTTPStatusCode != 200) {
     NSLog(@"HTTP status code = %ld", (long)HTTPStatusCode);
     }
         else
         {
             
         }
     // Call the completion handler with the returned data on the main thread.
     //[[NSOperationQueue mainQueue] addOperationWithBlock:^{
     //completionHandler(data);
     //}];
     }
     
     }];
     
     [postDataTask resume];
     
}

# pragma mark - Button actions
- (IBAction)checkoutAction:(id)sender
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Checkout" message:@"Enter your name:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save",nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alert setTag:11];
    [alert show];
}


#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( [alertView tag] == 11 ) {
        //Checkout book
        if( buttonIndex == 1 )
        {
            NSString *enterTxt=[[alertView textFieldAtIndex:0] text];
            
            if([enterTxt length]!=0)
            {
                userName=enterTxt;
                [self checkoutBook];
                if(errorchkout==nil)
                {
                    [self showSavedMessage:YES];
                    self.checkoutLabel.text=[NSString stringWithFormat:@"%@@%@",userName,dateString];
                }
                else
                {
                    [self showSavedMessage:NO];

                }
            }
        }
    }
}

#pragma mark save successful message
- (void)showSavedMessage:(BOOL)bin
{
    if(bin)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [hud setMode:MBProgressHUDModeText];
        [hud setLabelText:[NSString stringWithFormat:@"Checkout successful: %@", self.bookName]];
        [hud setMargin:10.f];
        [hud setYOffset:150.f];
        [hud hide:YES afterDelay:3];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [hud setMode:MBProgressHUDModeText];
        [hud setLabelText:[NSString stringWithFormat:@"Checkout failed: %@", self.bookName]];
        [hud setMargin:10.f];
        [hud setYOffset:150.f];
        [hud hide:YES afterDelay:3];
    }
    
}

@end
