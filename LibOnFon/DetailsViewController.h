//
//  DetailsViewController.h
//  LibOnFon
//
//  Created by Kunal Shrivastava on 12/30/14.
//  Copyright (c) 2014 Kunal Shrivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailsViewControllerDelegate <NSObject>
- (void)fromDetailsViewController;
@end

@interface DetailsViewController : UIViewController<UIAlertViewDelegate>






@property (nonatomic, weak) id <DetailsViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *booknameLabel;
@property (strong, nonatomic) IBOutlet UILabel *publisherLabel;
@property (strong, nonatomic) IBOutlet UILabel *tagsLabel;
@property (strong, nonatomic) IBOutlet UILabel *checkoutLabel;


@property (strong, nonatomic) NSString *bookName;
@property (strong, nonatomic) NSString *publisher;
@property (strong, nonatomic) NSString *tags;
@property (strong, nonatomic) NSString *checkout;
@property (strong, nonatomic) NSString *bookurl;



@end
