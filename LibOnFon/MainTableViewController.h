//
//  MainTableViewController.h
//  LibOnFon
//
//  Created by Kunal Shrivastava on 12/30/14.
//  Copyright (c) 2014 Kunal Shrivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "DetailsViewController.h"
#import "UpdateViewController.h"
#import "AddViewController.h"

@interface MainTableViewController : UITableViewController <MBProgressHUDDelegate,DetailsViewControllerDelegate,AddViewControllerDelegate>
{
    MBProgressHUD *HUD;
    NSMutableArray* books;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (strong, nonatomic) IBOutlet UILabel *errorLabel;
@property (strong) NSIndexPath *deleteIndex;
- (IBAction)sweepButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sweepButtoncust;

@end
